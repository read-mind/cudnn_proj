import matplotlib
matplotlib.use('Agg')
from matplotlib.backends.backend_pdf import PdfPages

import cPickle
import os
import glob
import numpy as np
import h5py
from sklearn import preprocessing
import scipy.signal
import matplotlib.pyplot as plt
import utils
import gflags
import sys

FLAGS = gflags.FLAGS
gflags.DEFINE_string('testfiles_dir', 'feats/test/adtCafe/', 'test feats and targets')
gflags.DEFINE_string('outdir', 'feats/test/pred/', 'prediction outdir')
gflags.DEFINE_string('deploy_proto', '', 'prototxt')
gflags.DEFINE_string('snapshot_prefix', '', 'model prefix')
gflags.DEFINE_string('normalizer_file', '', 'normalizer')
gflags.DEFINE_string('glob_regex', '*', 'more criterion to filter test files')
gflags.DEFINE_string('compress_feats', 'none', 'none|log|root3|root15')
gflags.DEFINE_string('caffe_path', '' , 'caffe install path')
gflags.DEFINE_bool('is_delta', False, 'do delta?')
gflags.DEFINE_bool('is_arma', True, 'do ARMA?')
gflags.DEFINE_bool('is_flatten', True, 'new feature layout')
gflags.DEFINE_bool('oversample', False, 'oversample')
gflags.DEFINE_integer('input_flank', 2 , 'left/right context window size')
gflags.DEFINE_integer('output_flank', 0 , 'left/right context window size')
gflags.DEFINE_integer('feat_dim', 123 , 'expected input bin dim')
gflags.DEFINE_integer('target_dim', 64 , 'expected target bin dim')
gflags.DEFINE_integer('snapshot_id', 10000 , 'which model')

def main(argv):
	try:
		argv = FLAGS(argv)
	except gflags.FlagsError, e:
		print e
		sys.exit(1)

        caffe_root = FLAGS.caffe_path
        print 'using caffe install at:', caffe_root
        sys.path.insert(0, caffe_root + 'python')
        import caffe


	snapshot_id = FLAGS.snapshot_id
	MODEL_PROTO = FLAGS.deploy_proto
	MODEL_FILE = FLAGS.snapshot_prefix + '_iter_' + str(FLAGS.snapshot_id)
        #if ~os.path.isfile(MODEL_FILE):
        #     print 'add .caffemodel'
	MODEL_FILE = MODEL_FILE + '.caffemodel'
	print 'using model snapshot:%s' % MODEL_FILE
        try:
            caffe.set_phase_test()
        except:
            print ""
	net = caffe.Classifier(MODEL_PROTO, MODEL_FILE)
        try:
	    net.set_phase_test()
        except:
            print ""
        #net.set_mode_gpu()

	test_bin_dir = FLAGS.testfiles_dir
	feat_files_regex = FLAGS.glob_regex + '*feats*.bin'
	target_files_regex = FLAGS.glob_regex + '*sqrt_irm*.bin'
	test_feats = sorted(glob.glob(test_bin_dir + '/' + feat_files_regex))
	test_irms = sorted(glob.glob(test_bin_dir + '/' + target_files_regex))
	print 'found %d test files matching %s' % (len(test_feats), test_bin_dir + '/' + feat_files_regex)

	
	normalizer_file = FLAGS.normalizer_file
	with open(normalizer_file, 'rb') as fid:
	    normalizer = cPickle.load(fid)

	# prediction
        pdf = PdfPages('mask_' + FLAGS.snapshot_prefix.split('/')[-1]  +'.pdf')
	outdir = FLAGS.outdir
	prediction_list, test_y_list = [], []
        speech_idx = 0
	for feat_path, irm_path in zip(test_feats, test_irms):
		test_x = np.fromfile(feat_path, 'float32')
		test_x = np.reshape(test_x, [-1, FLAGS.feat_dim])
		# test_x = utils.CompressFeat(test_x, FLAGS.compress_feats)
		# test_x = utils.CompressFeat(test_x, 'none')
		#test_x = utils.CompressFeat(test_x, 'root15')
		test_x = utils.CompressFeat(test_x, FLAGS.compress_feats)
		test_x = utils.correct_MaybeDeltaMVAWindow(test_x, normalizer['my_mean'],
                                               normalizer['my_std'],
	                                       FLAGS.is_delta,
	                                       FLAGS.is_arma,
	                                       FLAGS.input_flank)

		# test_x_caffe = [test_x[i,:].reshape(-1,1,1) for i in xrange(test_x.shape[0])]
#		test_x_caffe = [test_x[i,:].reshape(1,1,-1) for i in xrange(test_x.shape[0])]
		test_x_caffe = [test_x[i,:].reshape(1,-1,1) for i in xrange(test_x.shape[0])]


		#print test_x_caffe[1].shape
		prediction = net.predict(test_x_caffe, oversample=FLAGS.oversample)
                
                #data_list =  [(k, v.data.shape, v.data) for k, v in net.blobs.items()]
                #w_list =  [(k, v[0].data.shape, v[0].data, v[1].data.shape, v[1].data) for k, v in net.params.items()]
                #cPickle.dump(data_list, open('data.pickle', 'wb')) 
                #cPickle.dump(w_list, open('w.pickle', 'wb')) 
                   
                
                #print net.params['ip1'][0].data.shape
                #print net.params['ip1'][1].data.shape

                #cPickle.dump(test_x_caffe, open('utterance.pickle', 'wb'))
                #exit()
		
		# vec2window
		if FLAGS.output_flank != 0:
			prediction = utils.vec2window(prediction, FLAGS.target_dim)

		# get vid
		vid = '.'.join(feat_path.split('/')[-1].split('.')[:2])
		pred_suffix = MODEL_FILE.split('/')[-1]
		outfile = outdir + vid + '.' + pred_suffix + '.pred'
		prediction.tofile(outfile)

                if speech_idx < 50:
                    fig, col = plt.subplots(2,1)
                    col[0].imshow(np.transpose(prediction))
                    test_y = np.fromfile(irm_path, 'float32')
                    test_y = np.reshape(test_y, [-1, FLAGS.target_dim])
                    col[1].imshow(np.transpose(test_y))
                    col[0].invert_yaxis()
                    col[1].invert_yaxis()
                    plt.suptitle('utterance ' + str(speech_idx))
                    pdf.savefig()
                    plt.close('all')
                    print prediction.shape, speech_idx
                elif speech_idx == 50:
                    pdf.close()

                speech_idx = speech_idx + 1


if __name__ == '__main__':
    main(sys.argv)
    print 'prediction done and dumped to %s.' % FLAGS.outdir
