import numpy as np
from numpy import matlib
import math
from scipy import signal
import time
import pyfftw
import scipy

pi = 3.141592653589793

af = np.asarray([2.34700000, 2.19000000, 2.05000000, 1.87900000, 1.72400000, 1.57900000, 1.51200000, 1.46600000, 1.42600000, 1.39400000, 1.37200000, 1.34400000, 1.30400000, 1.25600000, 1.20300000, 1.13500000, 1.06200000, 1.00000000, 0.96700000, 0.94300000, 0.93200000, 0.93300000, 0.93700000, 0.95200000, 0.97400000, 1.02700000, 1.13500000, 1.26600000, 1.50100000])

bf = np.asarray([0.00561000, 0.00527000, 0.00481000, 0.00404000, 0.00383000, 0.00286000, 0.00259000, 0.00257000, 0.00256000, 0.00255000, 0.00254000, 0.00248000, 0.00229000, 0.00201000, 0.00162000, 0.00111000, 0.00052000, 0.00000000, -0.00039000, -0.00067000, -0.00092000, -0.00105000, -0.00104000, -0.00088000, -0.00055000, 0.00000000, 0.00089000, 0.00211000, 0.00488000])

cf = np.asarray([74.30000000, 65.00000000, 56.30000000, 48.40000000, 41.70000000, 35.50000000, 29.80000000, 25.10000000, 20.70000000, 16.80000000, 13.80000000, 11.20000000, 8.90000000, 7.20000000, 6.00000000, 5.00000000, 4.40000000, 4.20000000, 3.70000000, 2.60000000, 1.00000000, -1.20000000, -3.60000000, -3.90000000, -1.10000000, 6.60000000, 15.30000000, 16.40000000, 11.60000000])

cf = np.asarray([74.30000000, 65.00000000, 56.30000000, 48.40000000, 41.70000000, 35.50000000, 29.80000000, 25.10000000, 20.70000000, 16.80000000, 13.80000000, 11.20000000, 8.90000000, 7.20000000, 6.00000000, 5.00000000, 4.40000000, 4.20000000, 3.70000000, 2.60000000, 1.00000000, -1.20000000, -3.60000000, -3.90000000, -1.10000000, 6.60000000, 15.30000000, 16.40000000, 11.60000000])

ff = np.asarray([20.00000000, 25.00000000, 31.50000000, 40.00000000, 50.00000000, 63.00000000, 80.00000000, 100.00000000, 125.00000000, 160.00000000, 200.00000000, 250.00000000, 315.00000000, 400.00000000, 500.00000000, 630.00000000, 800.00000000, 1000.00000000, 1250.00000000, 1600.00000000, 2000.00000000, 2500.00000000, 3150.00000000, 4000.00000000, 5000.00000000, 6300.00000000, 8000.00000000, 10000.00000000, 12500.00000000])

def fftconvolve_1d(in1, in2, mode="full"):
    #one dimensional fft-based convolution, code adapted from scipy.signal.fftconvolve

    in1 = np.asarray(in1)
    in2 = np.asarray(in2)

    s1 = np.array(in1.shape)
    s2 = np.array(in2.shape)
    size = s1 + s2 - 1

    # Always use 2**n-sized FFT
    fsize = 2 ** np.ceil(np.log2(size)).astype(int)

    ret = np.fft.irfft(np.fft.rfft(in1, fsize[1]) *
            np.fft.rfft(in2, fsize[1]), fsize[1])[:,0:in2.shape[1]]
    ret = ret.real

    return ret

def fast_fftconvolve_1d(in1, in2, mode="full"):
    #one dimensional fft-based convolution, code adapted from scipy.signal.fftconvolve
    #pyfftw.interfaces.cache.enable()

    in1 = np.asarray(in1)
    in2 = np.asarray(in2)

    s1 = np.array(in1.shape)
    s2 = np.array(in2.shape)
    size = s1 + s2 - 1

    # Always use 2**n-sized FFT
    fsize = 2 ** np.ceil(np.log2(size)).astype(int)

    in1_copy = pyfftw.n_byte_align_empty((in1.shape[0],in1.shape[1]), 16, 'float32')
    in1_copy[:] = in1

    in2_copy = pyfftw.n_byte_align_empty((in2.shape[0],in2.shape[1]), 16, 'float32')
    in2_copy[:] = in2

    in1_copy = pyfftw.interfaces.numpy_fft.rfft(in1, fsize[1])
    in2_copy = pyfftw.interfaces.numpy_fft.rfft(in2, fsize[1])
    
    in3_fft = pyfftw.n_byte_align_empty((in1_copy.shape[0],in1_copy.shape[1]), 16, 'complex128')
    in3_fft[:] = np.asarray(in1_copy) * np.asarray(in2_copy)
    
    my_result = pyfftw.interfaces.numpy_fft.irfft(in3_fft, fsize[1])[:,0:in2.shape[1]]

    ret = my_result.real.astype(np.float32)
    return ret


def get_cochlea(in_sig, num_gf_ch=64, winLength=320):

    r = gammatone(in_sig, num_gf_ch, np.asarray([50, 8000]), 16e3)
    #r = gammatone(in_sig, 256, np.asarray([50, 8000]), 16e3)

    numChan, sigLength = r.shape #number of channels and input signal length
    winShift = winLength/2          #frame shift (default is half frame)
    increment = winLength/winShift  #special treatment for first increment-1 frames
    M = int(math.floor(sigLength/winShift))   #number of time frames

    #calculate energy for each frame in each channel
    a = np.zeros((numChan,M), dtype=np.float32)
    for m in range(M):      
        for i in range(numChan):
            if m < increment -1:       # shorter frame lengths for beginning frames
                a[i,m] = np.inner(r[i, 0:(m+1)*winShift] , r[i, 0:(m+1)*winShift])
            else:
                startpoint = (m + 1 - increment)*winShift;
                a[i,m] = np.dot(r[i,startpoint:startpoint+winLength] , r[i,startpoint:startpoint+winLength]);
    return a, r

def gammatone(in_sig, numChan=128, fRange=np.asarray([50,8000]), fs=16000.0):
    #Produce an array of filtered responses from a Gammatone filterbank.
    #numChan: number of filter channels.
    #fRange: frequency range.
    #fs: sampling frequency.
    
    filterOrder = 4    # filter order
    gL = 2048          # gammatone filter length or 128 ms for 16 kHz sampling rate
    
    sigLength = len(in_sig)  # input signal length
    
    phase= np.zeros((numChan,1))        # initial phases

    #erb_b = hz2erb(fRange);       # upper and lower bound of ERB

    erb_b = 21.4 * np.log10(4.37e-3 * fRange + 1)

    erb = np.linspace(erb_b[0], erb_b[1], numChan)

    #cf = erb2hz(erb);       # center frequency array indexed by channel

    center_f= (np.power(10, (erb/21.4)) - 1.0) / 4.37e-3

    b = 1.019*24.7*(4.37*center_f/1000+1);       # rate of decay or bandwidth

    ## Generating gammatone impulse responses with middle-ear gain normalization
    gt = np.zeros((numChan,gL))

    tmp_t = np.asarray(range(1,gL+1)) / fs


    for i in range(numChan):
        #loundness start
        freq = center_f[i]
        dB = 60
        
        j = 0
        while ff[j] < freq:
            j=j+1;

        afy = af[j-1]+(freq-ff[j-1])*(af[j]-af[j-1])/(ff[j]-ff[j-1]);
        bfy = bf[j-1]+(freq-ff[j-1])*(bf[j]-bf[j-1])/(ff[j]-ff[j-1]);
        cfy = cf[j-1]+(freq-ff[j-1])*(cf[j]-cf[j-1])/(ff[j]-ff[j-1]);

        loudness = 4.2+afy*(dB-cfy)/(1+bfy*(dB-cfy));       
        #loundness end

        gain = np.power(10,(loudness - 60)/20) / 3 * np.power(2*pi*b[i]/fs,4);    # loudness-based gain adjustments

        gt[i,:] = gain * pow(fs,3) * np.multiply(np.multiply( np.power(tmp_t, (filterOrder-1)) , np.exp(-2*pi*b[i] * tmp_t) ) ,  np.cos(2*pi*center_f[i] * tmp_t+phase[i]));
         
    #r = np.zeros((numChan, sigLength))
    #for i in range(numChan):
    #    r[i,:] = signal.fftconvolve(gt[i,:].reshape(gL,1), in_sig)[0:sigLength,0]

    r = fast_fftconvolve_1d(gt, matlib.repmat(np.transpose(in_sig),numChan,1)) # the most time consuming part

    return r

def inverse_gammatone(r, mask, numChan=64, fRange=np.asarray([50,8000]), fs=16000.0):
    filterOrder = 4    # filter order
    gL = 2048          # gammatone filter length or 128 ms for 16 kHz sampling rate
    
    sigLength = r.shape[1]  # input signal length
    phase= np.zeros((numChan,1))        # initial phases

    erb_b = 21.4 * np.log10(4.37e-3 * fRange + 1)
    erb = np.linspace(erb_b[0], erb_b[1], numChan)

    center_f= (np.power(10, (erb/21.4)) - 1.0) / 4.37e-3
    b = 1.019*24.7*(4.37*center_f/1000+1);       # rate of decay or bandwidth

    ## Generating gammatone impulse responses with middle-ear gain normalization
    gt = np.zeros((numChan,gL))
    midEarCoeff = np.zeros((numChan,1));#frequency-dependent mid-ear coefficients for inverse only
    tmp_t = np.asarray(range(1,gL+1)) / fs

    for i in range(numChan):
        #loundness start
        freq = center_f[i]
        dB = 60
        
        j = 0
        while ff[j] < freq:
            j=j+1;

        afy = af[j-1]+(freq-ff[j-1])*(af[j]-af[j-1])/(ff[j]-ff[j-1]);
        bfy = bf[j-1]+(freq-ff[j-1])*(bf[j]-bf[j-1])/(ff[j]-ff[j-1]);
        cfy = cf[j-1]+(freq-ff[j-1])*(cf[j]-cf[j-1])/(ff[j]-ff[j-1]);

        loudness = 4.2+afy*(dB-cfy)/(1+bfy*(dB-cfy));       
        #loundness end

        midEarCoeff[i,0] = np.power(10,(loudness - 60)/20); # for inverse only

        gain = np.power(10,(loudness - 60)/20) / 3 * np.power(2*pi*b[i]/fs,4);    # loudness-based gain adjustments

        gt[i,:] = gain * pow(fs,3) * np.multiply(np.multiply( np.power(tmp_t, (filterOrder-1)) , np.exp(-2*pi*b[i] * tmp_t) ) ,  np.cos(2*pi*center_f[i] * tmp_t+phase[i]));

    ## start inverse ##
    coswin = (1 + np.cos( 2 * np.pi * np.arange(0,320) / 320 - np.pi) ) / 2 #raised cosine window
    coswin = np.resize(coswin,(1,320))

    # time reverse filter output & normalize out mid-ear coefficients
    temp1 = np.divide(np.fliplr(r), matlib.repmat(midEarCoeff, 1, sigLength) )

    # second pass filtering via FFTFILT
    temp2 = fast_fftconvolve_1d(gt, temp1)

    # time reverse again & normalize out mid-ear coefficients
    temp1 = np.divide(np.fliplr(temp2), matlib.repmat(midEarCoeff, 1, sigLength) )

    winLength = 320
    winShift = 160
    increment = winLength/winShift
    numFrame = int(math.floor(sigLength/winShift)) #number of time frames
    mask = np.transpose(mask)

    resyn = np.zeros((1, sigLength))
    for i in range(numChan):
        weight = np.zeros((1, sigLength))
        for m in range(0, numFrame-increment/2+1):
            startpoint = m * winShift
            if m < increment/2: # shorter frame lengths for beginning frames
                weight[0,0:startpoint+winLength/2] = weight[0, 0:startpoint+winLength/2] + mask[i,m]*coswin[0, winLength/2-startpoint:]
            else:
                weight[0, startpoint-winLength/2:startpoint+winLength/2] = weight[0, startpoint-winLength/2:startpoint+winLength/2] + mask[i,m]*coswin[0,:]
        resyn[0,:] = resyn[0,:] + temp1[i,:] * weight[0,:]

    return resyn

def wiener(mat_s, mat_n):
    line, column = mat_s.shape
    mask = np.zeros((line, column))
    mask  = np.sqrt(np.divide(mat_s, (mat_s + mat_n)))
    return mask

def stft(x, framesamp, hopsamp):
    w = scipy.hamming(framesamp)
    X = scipy.array([scipy.fft(w*x[i:i+framesamp])
                     for i in range(0, len(x)-framesamp, hopsamp)])
    return X

def istft(X, samp, hopsamp):
    x = scipy.zeros(samp)
    framesamp = X.shape[1]
    for n,i in enumerate(range(0, len(x)-framesamp, hopsamp)):
        x[i:i+framesamp] += scipy.real(scipy.ifft(X[n]))
    return x

def get_resyn_log10_power(in_spec, mix_sig, framesamp=320, hopsamp=160):
    mix_X = stft(mix_sig, framesamp, hopsamp)
    mix_half_X = mix_X[:,0:161]
    ang_mix_half_X = np.angle(mix_half_X)

    mag_half_X = np.power(np.power(10, in_spec), 0.5)
    half_X = np.multiply(mag_half_X, np.exp(1j * ang_mix_half_X))
    X = np.hstack(( half_X, np.conjugate(np.fliplr(half_X[:,1:160])) ))

    x = istft(X, len(mix_sig), hopsamp)
    return x

def get_spec(in_sig, framesamp=320, hopsamp=160):
    X = stft(in_sig, framesamp, hopsamp)
    half_X = X[:,0:161]
    mag_half_X = np.absolute(half_X)
    power_half_X = np.power(mag_half_X, 2)
    return power_half_X
