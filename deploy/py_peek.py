import cPickle
import numpy as np
import matplotlib
matplotlib.use('Agg')
from matplotlib.backends.backend_pdf import PdfPages

import matplotlib.pyplot as plt
import utils

import scipy.io.wavfile as wavfile
import imp, sys
import time, math
import scipy.io

######### import moduels######################################
try:
    imp.find_module('scikits')
    import scikits.audiolab
except ImportError:
    print "can't play auido, scikits.auidolab missing"

try:
    imp.find_module('pyfftw')
    pyfftw_found = True
except ImportError:
    pyfftw_found = False

if pyfftw_found is True:
    from fast_cochleagram import get_cochlea, inverse_gammatone
    from fast_cochleagram import wiener
    from fast_cochleagram import get_spec, get_resyn_log10_power
    print 'pyfftw found, import fast_cochleagram.py'
else:
    from cochleagram import get_cochlea
    from cochleagram import wiener
    from fast_cochleagram import get_spec, get_resyn_log10_power
    print 'pyfftw missing, import cochleagram.py'
###############################################################

def load_model(w_path, normalizer_path):
    w = cPickle.load(open(w_path, 'rb'))
    with open(normalizer_path, 'rb') as fid:
        normalizer = cPickle.load(fid)
    return w, normalizer

def relu(X):
    return X * (X > 0)

def sigmoid(X):
    return 1 / (1 + np.exp(-1 * X))

def vec2window(x, expect_dim):
    m, d = x.shape
    win_side = (d/expect_dim - 1) / 2
    window = np.zeros((m, expect_dim))

    for t in xrange(win_side, m-win_side):
        chunck = x[t].reshape(-1, expect_dim)
        window[t-win_side : t+win_side+1, :] += chunck

    return window.astype('float32') / (2 * win_side + 1)

def get_wav(filename):
    rate, data = wavfile.read(filename)
    if rate != 16000:
        print "sampling rate is not equal to 16K:"
        exit()
    #only use the 1st channel if stereo
    if len(data.shape) > 1:
        data =  data[:,0]
    data = data.astype(np.float32)
    data = data / 32768 #convert PCM int16 to float
    return data

def get_features(utterance_path):
    mix = get_wav(utterance_path)
    constant = 5*10e6
    c = math.sqrt(constant * len(mix) / np.inner(mix,mix))
    mix = mix * c
    mix_cochlea, gamma_r = get_cochlea(mix)
    mix_cochlea = np.power(np.transpose(mix_cochlea), 1.0/15.0)
    return mix_cochlea, gamma_r

def postprocess_features(normalizer, input_feat):
    #test_x = np.fromfile(feat_path, 'float32')
    #test_x = np.reshape(test_x, [-1, 64])
    test_x = utils.correct_MaybeDeltaMVAWindow(input_feat, normalizer['my_mean'],\
             normalizer['my_std'], False, True, 11)
    return test_x

def forward(w, feat):
    batch_size =  feat.shape[0]
    res = np.dot(feat, np.transpose(w[0][2][0,0,:,:])) + np.tile(w[0][4][0,0,:,:], (batch_size,1))
    res = relu(res)
    
    res = np.dot(res, np.transpose(w[1][2][0,0,:,:])) + np.tile(w[1][4][0,0,:,:], (batch_size,1))
    res = relu(res)
    
    res = np.dot(res, np.transpose(w[2][2][0,0,:,:])) + np.tile(w[2][4][0,0,:,:], (batch_size,1))
    res = relu(res)
    
    res = np.dot(res, np.transpose(w[3][2][0,0,:,:])) + np.tile(w[3][4][0,0,:,:], (batch_size,1))
    res = relu(res)
    
    res = np.dot(res, np.transpose(w[4][2][0,0,:,:])) + np.tile(w[4][4][0,0,:,:], (batch_size,1))
    res = relu(res)
    
    res = np.dot(res, np.transpose(w[5][2][0,0,:,:])) + np.tile(w[5][4][0,0,:,:], (batch_size,1))
    res = sigmoid(res)
    
    res = vec2window(res, 64)
    return res

def synthesis(r, mask):
    #raised cosine window
    coswin = (1 + np.cos( 2 * np.pi * np.arange(0,320) / 320 - np.pi) ) / 2

    mask = np.transpose(mask)
    numChan = mask.shape[0]
    M = mask.shape[1]
    #calculate energy for each frame in each channel
    a = np.zeros((numChan,M), dtype=np.float32)
    for m in range(M):
        for i in range(numChan):
            if m < increment -1:       # shorter frame lengths for beginning frames
                a[i,m] = np.inner(r[i, 0:(m+1)*winShift] , r[i, 0:(m+1)*winShift])
            else:
                startpoint = (m + 1 - increment)*winShift;
                a[i,m] = np.dot(r[i,startpoint:startpoint+winLength] , r[i,startpoint:startpoint+winLength]);

def sound(to_play, fs=16000):
    try:
        imp.find_module('scikits')

        scikits.audiolab.play(to_play / np.max(np.abs(to_play)), fs)
    except ImportError:
        print "can't play auido"


def separation(w, normalizer, mix_path):
    time1 = time.time()
    input_feat, gamma_r = get_features(mix_path)
    feat = postprocess_features(normalizer, input_feat)
    mask = forward(w, feat)
    time2 = time.time() #timer
    print '%0.3f ms' % ((time2-time1)*1000.0)

    #sound(inverse_gammatone(gamma_r, mask))

    pdf = PdfPages(mix_path +'.pdf')

    plt.subplot(2, 2, 1)
    n_s = get_wav(mix_path)
    plt.plot(n_s)
    plt.xticks(np.arange(0, len(n_s)+1, 160*100))
    plt.title('noisy speech')

    plt.subplot(2, 2, 3)
    sep_s = inverse_gammatone(gamma_r, mask)[0,:]
    sep_s = np.divide(sep_s, np.max(np.abs(sep_s)))
    plt.plot(sep_s)
    plt.xticks(np.arange(0, len(sep_s)+1, 160*100))
    plt.title('separated speech')

    plt.subplot(2, 2, 2)
    plt.imshow(np.transpose(input_feat))
    plt.gca().invert_yaxis()
    plt.title('noisy features')

    plt.subplot(2, 2, 4)
    plt.imshow(np.transpose(mask))
    plt.gca().invert_yaxis()
    plt.title('estimated mask')

    #plt.show()
    
    pdf.savefig()
    pdf.close()

    sound(n_s)
    sound(sep_s)

def main(argv):
    w, normalizer = load_model('w.pickle', 'idea640k_train_mva_t11t2_normalizer.pickle')

    for i in range(0,1):
        mask = separation(w, normalizer, argv[0])

if __name__ == "__main__":
    main(sys.argv[1:])
