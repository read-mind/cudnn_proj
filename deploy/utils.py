import os
import numpy as np
import scipy.signal
from sklearn import preprocessing

def my_normalize(input, my_mean, my_std):
    #print 'mean:'
    #print my_mean
    #print 'std:'
    #print my_std
    output = np.divide( input - np.tile(my_mean,(input.shape[0],1)), np.tile(my_std,(input.shape[0],1)) )
    return output

def lsnr2wiener(lsnr_mask):
    return 1.0 / (1.0 + np.exp(-lsnr_mask * np.log(10.0)/10.0))

def MeanVarNormalize(X):
    scaler = preprocessing.StandardScaler(copy=False).fit(X)
    return scaler.transform(X), scaler

def ApplyARMA(X, order=2, dtype='float32'):
    if order == 0:
    	return X
    
    Z1, Z2 = np.tile(X[0,:], (order, 1)), np.tile(X[-1,:], (order, 1))        
    X_ARMA = np.vstack([Z1, X, Z2])
    
    for t in xrange(order, X_ARMA.shape[0]-order):
        X_ARMA[t, :] = (X_ARMA[t-order : t+order+1, :].sum(axis=0)) / (2*order + 1)
            
    return X_ARMA[order:X_ARMA.shape[0]-order, :].astype(dtype)

def MakeWindowFeat(X, side_size, dtype='float32'):
    if side_size == 0:
        return X    
    X_win = np.zeros(shape=(X.shape[0], X.shape[1] * (2*side_size+1)), dtype=dtype)
    Z = np.zeros(shape=(side_size, X.shape[1]), dtype=dtype)
    X_padded = np.vstack([Z, X, Z])
            
    for t in xrange(side_size, X_padded.shape[0]-side_size):
        X_win[t-side_size, :] = X_padded[t-side_size:t+side_size+1,:].flatten()    
    return X_win

#for subband
def MakeWindowFeat2(X, side_size, dtype='float32'):
    if side_size == 0:
        return X
    X_win = np.zeros(shape=(X.shape[0], X.shape[1] * (2*side_size+1)), dtype=dtype)
    Z = np.zeros(shape=(side_size, X.shape[1]), dtype=dtype)
    X_padded = np.vstack([Z, X, Z])

    for t in xrange(side_size, X_padded.shape[0]-side_size):
        X_win[t-side_size, :] = X_padded[t-side_size:t+side_size+1,:].T.flatten()
    return X_win

def vec2window(x, expect_dim):
    m, d = x.shape
    win_side = (d/expect_dim - 1) / 2
    window = np.zeros((m, expect_dim))
    
    for t in xrange(win_side, m-win_side):
        chunck = x[t].reshape(-1, expect_dim)
        window[t-win_side : t+win_side+1, :] += chunck
    
    return window.astype('float32') / (2 * win_side + 1)

#when convert multiple target to single target, use weighting curve
def vec2window_weight(x, expect_dim):
    m, d = x.shape
    win_side = (d/expect_dim - 1) / 2

    curve_left_mid = np.linspace(0.5, 1, num=(win_side+1))
    #curve_left_mid = np.asarray([0, 0, 1])
    curve_right = curve_left_mid[0:win_side][::-1]
    total_curve = np.hstack((curve_left_mid, curve_right))
    print total_curve
    final_curve = np.zeros((d/expect_dim, expect_dim))
    for i in range(0,d/expect_dim):
        final_curve[i, :] = np.tile(total_curve[i],(1,expect_dim))

    window = np.zeros((m, expect_dim))

    for t in xrange(win_side, m-win_side):
        chunck = x[t].reshape(-1, expect_dim)
        window[t-win_side : t+win_side+1, :] += np.multiply(chunck , final_curve)

    return window.astype('float32') / np.sum(total_curve)


#for subband
def vec2window2(x, expect_dim):
    m, d = x.shape
    win_side = (d/expect_dim - 1) / 2
    window = np.zeros((m, expect_dim))

    for t in xrange(win_side, m-win_side):
        # chunck = x[t].reshape(-1, expect_dim)
        chunck = x[t].reshape(expect_dim, -1).T
        window[t-win_side : t+win_side+1, :] += chunck

    return window.astype('float32') / (2 * win_side + 1)


def vec2window_geomean(x, expect_dim):
    m, d = x.shape
    win_side = (d/expect_dim - 1) / 2
    window = np.ones((m, expect_dim))
    
    for t in xrange(win_side, m-win_side):
        chunck = x[t].reshape(-1, expect_dim)
        window[t-win_side : t+win_side+1, :] *= chunck
    
    return np.power(window.astype('float32'), 1.0/(2*win_side+1))

def MakeDeltas(X, win_size=9):
    #  assuming X is m x d
    hlen = win_size / 2
    win = range(hlen, -hlen-1, -1)
    
    Z1, Z2 = np.tile(X[0, :], (hlen, 1)), np.tile(X[-1, :], (hlen, 1))        
    X_padded = np.vstack([Z1, X, Z2]) 

    d = scipy.signal.lfilter(win, 1 , X_padded, axis=0) 
    return d[2*hlen : 2*hlen+X.shape[0], :]

def DeltaMVAWindow(X, side_size, scaler=None):
    X = np.hstack((X, MakeDeltas(X)))
    if scaler is None:
        X, normalize_coef = MeanVarNormalize(X)
        X = ApplyARMA(X, order=2)
        X = MakeWindowFeat(X, side_size)
        return X, normalize_coef
    else:
        X = ApplyARMA(scaler.transform(X))
        X = MakeWindowFeat(X, side_size)
        return X

def MaybeDeltaMVAWindow(X, is_delta=False, is_arma=False, side_size=0, scaler=None):
    if is_delta:
        X = np.hstack((X, MakeDeltas(X)))
    if scaler is None:
        X, normalize_coef = MeanVarNormalize(X)
        if is_arma:
            X = ApplyARMA(X, order=2)
        if side_size != 0:
            X = MakeWindowFeat(X, side_size)
        return X, normalize_coef
    else:
        X = scaler.transform(X)
        if is_arma:
            X = ApplyARMA(X)
        if side_size != 0:
            X = MakeWindowFeat(X, side_size)
        return X

def correct_MaybeDeltaMVAWindow(X, my_mean, my_std, is_delta=False, is_arma=False, side_size=0):
    if is_delta:
        X = np.hstack((X, MakeDeltas(X)))
    X = my_normalize(X, my_mean, my_std)
    if is_arma:
        X = ApplyARMA(X)
    if side_size != 0:
        X = MakeWindowFeat(X, side_size)
    return X

#for subband
def correct_MaybeDeltaMVAWindow2(X, my_mean, my_std, is_delta=False, is_arma=False, side_size=0):
    if is_delta:
        X = np.hstack((X, MakeDeltas(X)))
    X = my_normalize(X, my_mean, my_std)
    if is_arma:
        X = ApplyARMA(X)
    if side_size != 0:
        X = MakeWindowFeat2(X, side_size)
    return X




def CompressFeat(X, compress_type):
    if compress_type == 'log':
        X = np.log(X + 1e-3)
    elif compress_type == 'root3':
        X = np.power(X+1e-3, 1.0/3.0)
    elif compress_type == 'root15':
        X = np.power(X+1e-3, 1.0/15.0)
    
    return X

def get_mean_std_batch(X, batch_size=500000, compressor=None):
    mean = np.zeros((X.shape[1],)).astype('float32')
    std = np.zeros((X.shape[1],)).astype('float32')
    # ss = np.zeros((X.shape[1],)).astype('float32')

    count = X.shape[0]
    num_batches = int(np.ceil(X.shape[0]/float(batch_size)))
    for i in xrange(num_batches):
        block = X[i*batch_size: (i+1)*batch_size, :]
        if compressor:
            mean += np.sum(compressor(block), axis=0)/count
        else:
            mean += np.sum(block, axis=0)/count
        # ss += np.sum(block, axis=0)

    for i in xrange(num_batches):
        block = X[i*batch_size: (i+1)*batch_size, :]
        if compressor:
            std += np.sum(np.square(compressor(block)-mean), axis=0)/count
        else:
            std += np.sum(np.square(block-mean), axis=0)/count
    std = np.sqrt(std)

    return mean, std
    # return ss, std

# debug code to show that batching may cause precision issues (even sum do not match)
# but swithcing to float64 matches np.sum
# def get_mean_std_batch(X, batch_size=500000, compressor=None):
#     # print 'haha'
#     # mean = np.zeros((X.shape[1],)).astype('float32')
#     # std = np.zeros((X.shape[1],)).astype('float32')
#     mean = np.zeros((X.shape[1],)).astype('float64')
#     ss = np.zeros((X.shape[1],)).astype('float64')
#     std = np.zeros((X.shape[1],)).astype('float64')
#     ssvec = []

#     count = X.shape[0]
#     print count
#     num_batches = int(np.ceil(X.shape[0]/float(batch_size)))
#     print num_batches
#     for i in xrange(num_batches):
#         # if (i+1)*batch_size <= count:
#         #     block = X[i*batch_size: (i+1)*batch_size, :]
#         # else:
#         #     block = X[i*batch_size: count, :]
#         block = X[i*batch_size: (i+1)*batch_size, :].astype('float64')

#         print block.shape
#         # count += block.shape[0]
#         if compressor:
#             mean += np.sum(compressor(block), axis=0)/count
#         else:
#             mean += np.sum(block, axis=0)/count

#         ssvec.append(np.sum(block, axis=0).astype('float64'))
#         ss += np.sum(block, axis=0).astype('float64')

#         # mean += np.sum(compressor(block), axis=0)
#     # mean = mean / count

#     # for i in xrange(num_batches):
#     #     block = X[i*batch_size: (i+1)*batch_size, :]
#     #     if compressor:
#     #         std += np.sum(np.square(compressor(block)-mean), axis=0)/count
#     #     else:
#     #         std += np.sum(np.square(block-mean), axis=0)/count
#     # std = np.sqrt(std)

#     return mean, ss, ssvec
