/**
 * This example demonstrates how to use CUDNN library to implement forward
 * pass. The sample loads weights and biases from trained network,
 * takes a few images of digits and recognizes them. The network was trained on 
 * the MNIST dataset using Caffe. The network consists of two 
 * convolution layers, two pooling layers, one relu and two 
 * fully connected layers. Final layer gets processed by Softmax. 
 * cublasSgemv is used to implement fully connected layers.
 */

#include <sstream>
#include <fstream>
#include <stdlib.h>

#include <cublas_v2.h>
#include <cudnn.h>
 #include <cuda.h>


#include "ImageIO.h"


#define value_type float

#define IMAGE_H 1
#define IMAGE_W 1
#define CHANNEL_SIZE 2
#define BATCH_SIZE 4

const char *first_image = "image.bin";

const char *ip1_bin = "IP1.bin";
const char *ip1_bias_bin = "IP1_b.bin";
const char *ip2_bin = "IP2.bin";
const char *ip2_bias_bin = "IP2_b.bin";

/********************************************************
 * Prints the error message, and exits
 * ******************************************************/

#define EXIT_WAIVED 0

#define FatalError(s) {                                                \
    std::stringstream _where, _message;                                \
    _where << __FILE__ << ':' << __LINE__;                             \
    _message << std::string(s) + "\n" << __FILE__ << ':' << __LINE__;\
    std::cerr << _message.str() << "\nAborting...\n";                  \
    cudaDeviceReset();                                                 \
    exit(EXIT_FAILURE);                                                \
}

#define checkCUDNN(status) {                                           \
    std::stringstream _error;                                          \
    if (status != CUDNN_STATUS_SUCCESS) {                              \
      _error << "CUDNN failure: " << status;                           \
      FatalError(_error.str());                                        \
    }                                                                  \
}

#define checkCudaErrors(status) {                                      \
    std::stringstream _error;                                          \
    if (status != 0) {                                                 \
      _error << "Cuda failure: " << status;                            \
      FatalError(_error.str());                                        \
    }                                                                  \
}

void get_path(std::string& sFilename, const char *fname, const char *pname)
{
    sFilename = (std::string("data/") + std::string(fname));
}

void printDeviceVector(int size, value_type* vec_d)
{
    value_type *vec;
    vec = new value_type[size];
    cudaDeviceSynchronize();
    cudaMemcpy(vec, vec_d, size*sizeof(value_type), cudaMemcpyDeviceToHost);
    for (int i = 0; i < size; i++)
    {
        std::cout << vec[i] << " ";
    }
    std::cout << std::endl;
    delete [] vec;
}

void print_device_vec(int size_x, int size_y, value_type* vec_d)
{
    int size = size_x * size_y;
    value_type *vec;
    vec = new value_type[size];
    cudaDeviceSynchronize();
    cudaMemcpy(vec, vec_d, size*sizeof(value_type), cudaMemcpyDeviceToHost);
    for (int i = 0; i < size_x; i++)
    {
        for (int j = 0; j < size_y; j++)
        {
            std::cout << vec[i*size_y + j] << " ";
        }
        std::cout << std::endl;
    }
    std::cout << std::endl;
    delete [] vec;
}


void print_host_vec(int size_x, int size_y, value_type* vec)
{
    int size = size_x * size_y;
    for (int i = 0; i < size_x; i++)
    {
        for (int j = 0; j < size_y; j++)
        {
            std::cout << vec[i*size_y + j] << " ";
        }
        std::cout << std::endl;
    }
    std::cout << std::endl;
}

struct Layer_t
{
    int inputs;
    int outputs;
    value_type *data_h, *data_d;
    value_type *bias_h, *bias_d;
    Layer_t() : data_h(NULL), data_d(NULL), bias_h(NULL), bias_d(NULL), 
                inputs(0), outputs(0) {};
    Layer_t(int _inputs, int _outputs, const char* fname_weights,
            const char* fname_bias, const char* pname = NULL)
                  : inputs(_inputs), outputs(_outputs)
    {
        std::string weights_path, bias_path;
        if (pname != NULL)
        {
            get_path(weights_path, fname_weights, pname);
            get_path(bias_path, fname_bias, pname);
        }
        else
        {
            weights_path = fname_weights; bias_path = fname_bias;
        }
        std::cout <<  weights_path << " inputs=" << inputs << " outputs=" << outputs << std::endl;
        readBinaryFile(weights_path.c_str(), inputs * outputs, 
                        &data_h, &data_d);

        std::cout <<  bias_path << " outputs=" << outputs <<std::endl;
        readBinaryFile(bias_path.c_str(), outputs, &bias_h, &bias_d);
    }
    ~Layer_t()
    {
        delete [] data_h;
        checkCudaErrors( cudaFree(data_d) );
    }
private:
    void readBinaryFile(const char* fname, int size, value_type** data_h, value_type** data_d)
    {
        std::ifstream dataFile (fname, std::ios::in | std::ios::binary);
        std::stringstream error_s;
        if (!dataFile)
        {
            error_s << "Error opening file " << fname; 
            FatalError(error_s.str());
        }
        int size_b = size*sizeof(value_type);
        *data_h = new value_type[size];
        if (!dataFile.read ((char*) *data_h, size_b)) 
        {
            error_s << "Error reading file " << fname; 
            FatalError(error_s.str());
        }
        checkCudaErrors( cudaMalloc(data_d, size_b) );
        checkCudaErrors( cudaMemcpy(*data_d, *data_h,
                                    size_b,
                                    cudaMemcpyHostToDevice) );
    }
};


class network_t
{
    cudnnDataType_t dataType;
    cudnnTensorFormat_t tensorFormat;
    cudnnHandle_t cudnnHandle;
    cudnnTensorDescriptor_t srcTensorDesc, dstTensorDesc, biasTensorDesc;
    cudnnFilterDescriptor_t filterDesc;
    cublasHandle_t cublasHandle;
    void createHandles()
    {
        checkCUDNN( cudnnCreate(&cudnnHandle) );
        checkCUDNN( cudnnCreateTensorDescriptor(&srcTensorDesc) );
        checkCUDNN( cudnnCreateTensorDescriptor(&dstTensorDesc) );
        checkCUDNN( cudnnCreateTensorDescriptor(&biasTensorDesc) );
        checkCUDNN( cudnnCreateFilterDescriptor(&filterDesc) );

        checkCudaErrors( cublasCreate(&cublasHandle) );
    }
    void destroyHandles()
    {
        checkCUDNN( cudnnDestroyFilterDescriptor(filterDesc) );
        checkCUDNN( cudnnDestroyTensorDescriptor(srcTensorDesc) );
        checkCUDNN( cudnnDestroyTensorDescriptor(dstTensorDesc) );
        checkCUDNN( cudnnDestroyTensorDescriptor(biasTensorDesc) );
        checkCUDNN( cudnnDestroy(cudnnHandle) );

        checkCudaErrors( cublasDestroy(cublasHandle) );
    }
  public:
    network_t()
    {
        dataType = CUDNN_DATA_FLOAT;
        tensorFormat = CUDNN_TENSOR_NCHW;
        createHandles();    
    };
    ~network_t()
    {
        destroyHandles();
    }
    void resize(int size, value_type **data)
    {
        if (*data != NULL)
        {
            checkCudaErrors( cudaFree(*data) );
        }
        checkCudaErrors( cudaMalloc(data, size*sizeof(value_type)) );
    }

    void fullyConnectedForward(const Layer_t& ip,
                          int n, int c, int h, int w,
                          value_type* srcData, value_type** dstData)
    {
        int dim_x = c*h*w;
        int dim_y = ip.outputs;
        resize(n * dim_y, dstData);

        float f = 1.0f;
        cuMemsetD32(reinterpret_cast<CUdeviceptr>(*dstData), reinterpret_cast<int&>(f),n * dim_y);

        value_type alpha = value_type(1), beta = value_type(1);
        value_type alpha0 = value_type(0), beta0 = value_type(0);

        checkCudaErrors( cublasSgemm(cublasHandle, CUBLAS_OP_N, CUBLAS_OP_N, 
                                      dim_y, n, 1, 
                                      &alpha, 
                                      ip.bias_d, n, 
                                      *dstData, 1, 
                                      &beta0, 
                                      *dstData, dim_y) );

        std::cout << "bias dstData\n";
        print_device_vec(n, dim_y, *dstData);

        std::cout << "srcData " << n << "x"<< dim_x << "\n";
        print_device_vec(n, dim_x, srcData);

        std::cout << "weight " << dim_x<< "x"<< dim_y <<"\n";
        print_device_vec(dim_x, dim_y, ip.data_d);

        checkCudaErrors( cublasSgemm(cublasHandle, CUBLAS_OP_N, CUBLAS_OP_N, 
                                      dim_y, n, dim_x, 
                                      &alpha, 
                                      ip.data_d, dim_y, 
                                      srcData, dim_x, 
                                      &beta, 
                                      *dstData, dim_y) )

        std::cout << "RESULT: dstData\n";
        print_device_vec(n, dim_y, *dstData);
    }


    void softmaxForward(int n, int c, int h, int w, value_type* srcData, value_type** dstData)
    {
        resize(n*c*h*w, dstData);

        checkCUDNN( cudnnSetTensor4dDescriptor(srcTensorDesc,
                                                tensorFormat,
                                                dataType,
                                                n, c,
                                                h,
                                                w) );
        checkCUDNN( cudnnSetTensor4dDescriptor(dstTensorDesc,
                                                tensorFormat,
                                                dataType,
                                                n, c,
                                                h,
                                                w) );
        value_type alpha = value_type(1);
        value_type beta  = value_type(0);
        checkCUDNN( cudnnSoftmaxForward(cudnnHandle,
                                          CUDNN_SOFTMAX_ACCURATE ,
                                          CUDNN_SOFTMAX_MODE_INSTANCE,
                                          &alpha,
                                          srcTensorDesc,
                                          srcData,
                                          &beta,
                                          dstTensorDesc,
                                          *dstData) );
    }

    void activationForward(int n, int c, int h, int w, value_type* srcData, value_type** dstData)
    {        
        resize(n*c*h*w, dstData);
        checkCUDNN( cudnnSetTensor4dDescriptor(srcTensorDesc,
                                                tensorFormat,
                                                dataType,
                                                n, c,
                                                h,
                                                w) );
        checkCUDNN( cudnnSetTensor4dDescriptor(dstTensorDesc,
                                                tensorFormat,
                                                dataType,
                                                n, c,
                                                h,
                                                w) );
        value_type alpha = value_type(1);
        value_type beta  = value_type(0);
        checkCUDNN( cudnnActivationForward(cudnnHandle,
                                            CUDNN_ACTIVATION_RELU,
                                            &alpha,
                                            srcTensorDesc,
                                            srcData,
                                            &beta,
                                            dstTensorDesc,
                                            *dstData) );  
    }


    void read_bin(const char* fname, int size, value_type* data_h){
        std::ifstream dataFile (fname, std::ios::in | std::ios::binary);
        std::stringstream error_s;
        if (!dataFile)
        {
            error_s << "Error opening file " << fname;
            FatalError(error_s.str());
        }
        if (!dataFile.read ((char*) data_h, size * sizeof(value_type)))
        {
            error_s << "Error reading file " << fname;
            FatalError(error_s.str());
        }
    }


    int classify_example(const char* fname,
                          const Layer_t& ip1,
                          const Layer_t& ip2)
    {
        int n,c,h,w;
        n = BATCH_SIZE; c = CHANNEL_SIZE; h = IMAGE_H; w = IMAGE_H;
        value_type *srcData = NULL, *dstData = NULL;
        value_type imgData_h[n*c];

        // declare a host image object for an 8-bit grayscale image
        std::string sFilename(fname);
        std::cout << "Loading image " << sFilename << std::endl;
        read_bin(fname, n*c, imgData_h);

        //matrix on host also uses column-major
        print_host_vec(n, c, imgData_h);

        std::cout << "input data: n=" << n << " c=" << c <<std::endl;  
        std::cout << "Performing forward propagation ...\n";

        checkCudaErrors( cudaMalloc(&srcData, n*c*sizeof(value_type)) );
        checkCudaErrors( cudaMemcpy(srcData, imgData_h,
                                    n*c*sizeof(value_type),
                                    cudaMemcpyHostToDevice) );

        fullyConnectedForward(ip1, n, c, h, w, srcData, &dstData);
        activationForward(n, ip1.outputs, h, w, dstData, &srcData);
        

        std::cout << "After_act1: srcData\n";
        print_device_vec(n, ip1.outputs, srcData);

        
        std::cout << "############ LAYER 2 ###############\n";
        fullyConnectedForward(ip2, n, ip1.outputs, h, w, srcData, &dstData);

        std::cout << "After_linear2: dstData\n";
        print_device_vec(n, ip2.outputs, dstData);

        softmaxForward(n, ip2.outputs, h, w, dstData, &srcData);

        std::cout << "After_softmax: srcData\n";
        print_device_vec(n, ip2.outputs, srcData);
        


        /*
        fullyConnectedForward(ip2, n, c, h, w, srcData, &dstData);
        softmaxForward(n, c, h, w, dstData, &srcData);

        const int max_digits = 10;
        value_type result[max_digits];
        checkCudaErrors( cudaMemcpy(result, srcData, max_digits*sizeof(value_type), cudaMemcpyDeviceToHost) );
        int id = 0;
        for (int i = 1; i < max_digits; i++)
        {
            if (result[id] < result[i]) id = i;
        }
        
        std::cout << "Resulting weights from Softmax:" << std::endl;
        printDeviceVector(n*c*h*w, srcData);
        */

        int id = 1;
        checkCudaErrors( cudaFree(srcData) );
        checkCudaErrors( cudaFree(dstData) );
        return id;
    }
};

int main(int argc, char *argv[])
{
    if (argc > 2)
    {
        std::cout << "Test usage:\nmnistCUDNN [image]\nExiting...\n";
        exit(EXIT_FAILURE);
    }
    
    std::string image_path;
    network_t mnist;

    Layer_t   ip1(2,3,ip1_bin,ip1_bias_bin,argv[0]);

    //std::cout << "ip1 w:\n";
    //print_device_vec(2, 3, ip1.data_d);
    //std::cout << "ip1 b:\n";
    //print_device_vec(1, 3, ip1.bias_d);

    Layer_t   ip2(3,2,ip2_bin,ip2_bias_bin,argv[0]);
    
    if (argc == 1)
    {
        int i1,i2,i3;
        get_path(image_path, first_image, argv[0]);
        i1 = mnist.classify_example(image_path.c_str(), ip1, ip2);

        std::cout << "\nResult of classification: " << i1 << std::endl;
        if (i1 != 1)
        {
            std::cout << "\nTest failed!\n";
            FatalError("Prediction mismatch");
        }
        else
        {
            std::cout << "\nTest passed!\n";
        }
    }
    else
    {
        int i1 = mnist.classify_example(argv[1], ip1, ip2);
        std::cout << "\nResult of classification: " << i1 << std::endl;
    }
    cudaDeviceReset();
    exit(EXIT_SUCCESS);
}
