import cPickle
import numpy as np
import matplotlib.pyplot as plt

data = cPickle.load(open('data.pickle', 'rb'))
w = cPickle.load(open('w.pickle', 'rb'))

#print data
#print w

#plt.imshow(np.transpose(w['ip1'][0].data))
#plt.show()


prefix = './models/'

for i in range(0,6):

    print w[i][0]

    print np.transpose(w[i][2][0,0,:,:]).shape

    print np.transpose(w[i][4][0,0,0,:]).shape

######################################################################


for i in range(0,7):

    print data[i][0]

    print data[i][2][:,0,0,:].shape


print 'hello#######################'

def relu(X):
    return X * (X > 0)

def sigmoid(X):
    return 1 / (1 + np.exp(-1 * X))

def vec2window(x, expect_dim):
    m, d = x.shape
    win_side = (d/expect_dim - 1) / 2
    window = np.zeros((m, expect_dim))

    for t in xrange(win_side, m-win_side):
        chunck = x[t].reshape(-1, expect_dim)
        window[t-win_side : t+win_side+1, :] += chunck

    return window.astype('float32') / (2 * win_side + 1)


utterance = cPickle.load(open('utterance.pickle', 'rb'))
utterance = np.resize(utterance, (-1, 1472))
print utterance.shape
batch_size = 276

#batch_size =  256
#res = np.dot(data[0][2][:,0,0,:], np.transpose(w[0][2][0,0,:,:])) + np.tile(w[0][4][0,0,:,:], (batch_size,1))
res = np.dot(utterance, np.transpose(w[0][2][0,0,:,:])) + np.tile(w[0][4][0,0,:,:], (batch_size,1))
res = relu(res)

res = np.dot(res, np.transpose(w[1][2][0,0,:,:])) + np.tile(w[1][4][0,0,:,:], (batch_size,1))
res = relu(res)

res = np.dot(res, np.transpose(w[2][2][0,0,:,:])) + np.tile(w[2][4][0,0,:,:], (batch_size,1))
res = relu(res)

res = np.dot(res, np.transpose(w[3][2][0,0,:,:])) + np.tile(w[3][4][0,0,:,:], (batch_size,1))
res = relu(res)

res = np.dot(res, np.transpose(w[4][2][0,0,:,:])) + np.tile(w[4][4][0,0,:,:], (batch_size,1))
res = relu(res)

res = np.dot(res, np.transpose(w[5][2][0,0,:,:])) + np.tile(w[5][4][0,0,:,:], (batch_size,1))
res = sigmoid(res)

print res.shape
print res

print data[6][2][:,:,0,0].shape
print data[6][2][:,:,0,0]


res = vec2window(res, 64)
fig = plt.imshow(np.transpose(res))
plt.gca().invert_yaxis()
plt.show()

plt.imshow(vec2window(data[6][2][:,:,0,0],64))
plt.show()




#exit()
#
#prefix = './models/'
#
#for i in range(0,6):
#
#    print w[i][0]
#
#    print np.transpose(w[i][2][0,0,:,:]).shape
#    np.transpose(w[i][2][0,0,:,:]).tofile(prefix + w[i][0] + '_w.bin')
#
#    print np.transpose(w[i][4][0,0,0,:]).shape
#    np.transpose(w[i][4][0,0,0,:]).tofile(prefix + w[i][0] + '_b.bin')
